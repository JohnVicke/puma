const user = {
  givenName: "Viktor",
  familyName: "Malmedal",
  color: 0,
}

const primaryColors = {
  red: "#C55062",
  darkRed: "#6F1D2A",
  yellow: "#FFF858",
  darkYellow: "#E7E04B",
  blue: "#3D71F8",
  darkBlue: "#172E68",
}

const colorTranslationTable = {
  0: { primary: primaryColors.red, primaryDarken: primaryColors.darkRed },
  1: { primary: primaryColors.blue, primaryDarken: primaryColors.darkBlue },
  2: { primary: primaryColors.yellow, primaryDarken: primaryColors.darkYellow },
}

export const palette = {
  ...colorTranslationTable[user.color],
  black: "#1a1a1a",
  white: "#ffffff",
  bulletinBoard: "#F2FFA0",
  lightGray: "#EAEAEA",
  darkGray: "#C5C5C5",
  lightYellow: "#F2FFA0",

  darkYellow: "#E7E04B",
  blue: "#3D71F8",
  red: "#C55062",

}
