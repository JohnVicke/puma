/**
 * The available radius.
 * Feel free to add a new radius if needed. Append it last to the list in that case.
 *
 * 0 = tiny    - tiny radius
 * 1 = small   - small radius
 * 2 = medium  - medium radius
 * 3 = large   - large radius
 */
export const radius = [5, 10, 20, 30]
