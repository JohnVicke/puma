import React from "react"
import { observer } from "mobx-react-lite"
import { Image, ImageStyle, TextStyle, View, ViewStyle } from "react-native"
import { Button, Screen, Text } from "../../components"
// import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color, spacing } from "../../theme"
import { useNavigation } from "@react-navigation/native"
const idLogo = require("./id_hand.png")

/**
 * Since landingpage is a static page with nothing going on,
 * styling here is abit wonky (with absolutes etc).
 */

const ROOT: ViewStyle = {
  backgroundColor: color.palette.white,
  flex: 1,
}

const HEADER_VIEW: ViewStyle = {}

const BUTTON_GROUP: ViewStyle = {
  display: "flex",
  flexDirection: "column",
  marginHorizontal: spacing[8],
  top: -200,
}

const HEADERTEXT: TextStyle = {
  color: color.palette.black,
  fontWeight: "700",
  fontSize: 26,
  top: 125,
  marginLeft: spacing[8],
}

const HANDSTYLE: ImageStyle = {
  width: undefined,
  top: -100,
  resizeMode: "contain",
  transform: [{ rotate: "-17deg" }],
  marginHorizontal: spacing[5],
}

export const LandingScreen = observer(function LandingScreen() {
  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()
  // const rootStore = useStores()

  // TODO: Navigation doesnt work due to screens not being implemented yet
  const navigation = useNavigation()
  const goTo = (screen: string) => {
    navigation.navigate(screen)
  }

  return (
    <Screen style={ROOT} preset="fixed">
      <View style={HEADER_VIEW}>
        <Text style={HEADERTEXT} text="Interaktion" />
        <Text style={HEADERTEXT} text="& Design" />
      </View>
      <Image source={idLogo} style={HANDSTYLE} />
      <View style={BUTTON_GROUP}>
        <Button
          style={{ marginBottom: spacing[4] }}
          text="Logga in"
          preset="roundedFilled"
          onPress={() => goTo("login")}
        />
        <Button
          text="Skapa användare"
          preset="roundedOutlined"
          onPress={() => {
            navigation.navigate("createUser")
          }}
        />
      </View>
    </Screen>
  )
})
