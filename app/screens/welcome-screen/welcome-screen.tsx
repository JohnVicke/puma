import { useNavigation } from "@react-navigation/native"
import { observer } from "mobx-react-lite"
import React from "react"
import { View, ViewStyle } from "react-native"
import { Button } from "../../components"
import { spacing } from "../../theme"

const CONTAINER: ViewStyle = {
  backgroundColor: "#000",
  flex: 1,
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
}

const BUTTON: ViewStyle = {
  marginVertical: spacing[4],
}

export const WelcomeScreen = observer(function WelcomeScreen() {
  const navigation = useNavigation()
  const nextScreen = () => navigation.navigate("landingStack", { screen: "landing" })

  return (
    <View style={CONTAINER}>
      <Button text="hello world" preset="flat" style={BUTTON} onPress={nextScreen} />
    </View>
  )
})
