import React from "react"
import { observer } from "mobx-react-lite"
import { ViewStyle, FlatList, Image } from "react-native"
import { Screen } from "../../components"
import { ContactBox } from "../../components/contact-box/contact-box"
import { color } from "../../theme"

// NOTE: Only test to see if list is working
// Users should be read in from database in the same form
const contact1 = {
  id: "bd7acbea-c1b1-46c2-aed5-3ad53abb28ba",
  name: "Kalle Andersson",
  email: "kalle@gmail.com",
  phoneNr: "0738219284",
  color: "red",
}

const contact2 = {
  id: "3ac68afc-c605-48d3-a4f8-fbd91aa97f63",
  name: "Arnold Vikman",
  email: "mst@gmail.com",
  phoneNr: "073828383",
  color: "blue",
}
const contact3 = {
  id: "3ac68afc-c605-48d3-a4f8-fbd91aa97444",
  name: "McLovin",
  email: "mC@gmail.com",
  phoneNr: "073373737",
  color: "yellow",
}

const contactInfo = [contact2, contact1, contact3]

const ROOT: ViewStyle = {
  backgroundColor: color.palette.white,
  flex: 1,
  alignItems: "center",
}

// Change backgroundcolor depending on what group the person is in
const BLUE: ViewStyle = {
  backgroundColor: color.palette.blue,
  // backgroundColor: "#3D71F8",
}
const RED: ViewStyle = {
  backgroundColor: color.palette.red,
}
const YELLOW: ViewStyle = {
  backgroundColor: color.palette.darkYellow,
}

const setColor = (color: string) => {
  if (color === "blue") {
    return BLUE
  } else if (color === "red") {
    return RED
  } else {
    return YELLOW
  }
}

export const ContactScreen = observer(function ContactScreen() {
  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()
  // OR
  // const rootStore = useStores()

  // Pull in navigation via hook
  // const navigation = useNavigation()

  return (
    <Screen style={ROOT}>
      <FlatList
        data={contactInfo}
        keyExtractor={(item) => item.id}
        renderItem={({ item }) => (
          <ContactBox
            name={item.name}
            email={item.email}
            phoneNr={item.phoneNr}
            color={setColor(item.color)}
            ProfilePic={null}
          />
        )}
      />
    </Screen>
  )
})
