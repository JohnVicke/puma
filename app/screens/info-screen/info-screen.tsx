import React from "react"
import { observer } from "mobx-react-lite"
import { ViewStyle, View, Image, TextInput, ScrollView, TextStyle} from "react-native"
import { Screen, Text,TextField, BulletItem, InfoCard ,Lexicon} from "../../components"
// import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color, spacing, typography} from "../../theme"
import { useState } from "react"

const ROOT: ViewStyle = {
  backgroundColor: color.palette.white,
  flex: 1,
}

const SCROLLVIEW: ViewStyle = {
  paddingHorizontal: spacing[4],
  marginBottom: spacing[7], //padding/margin causes weird behaviour with scrolling and viewing
  paddingTop: spacing[4],
  display: "flex",
  flexDirection: "column",
}

const PADDING: ViewStyle = {
  marginBottom: spacing[5],
}
const LOGINBUTTON: ViewStyle = {
  ...PADDING,
  alignSelf: "flex-end",
}


const HEADER: TextStyle ={
  fontFamily: typography.primary,
  fontSize: 16,
  fontWeight: "bold",
  color: color.palette.primaryDarken,
  marginBottom: spacing[2]
}

export const InfoScreen = observer(function InfoScreen() {
  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()
  // OR
  // const rootStore = useStores()

  // Pull in navigation via hook
  // const navigation = useNavigation()
  return (
    <Screen style={ROOT} preset="scroll" unsafe={true}>
      <ScrollView style={SCROLLVIEW}>

        <View style={PADDING}>
          <Text style={HEADER} text="Allmän information"/>
          
          <InfoCard header="ID-Märket" text="Det är absolut förbjudet att sätta sin fot på ID-Märket..." />
          <InfoCard header="N0llefriden" text="Under mottagningen är det absolut förbjudet under några som helst 
            omständigheten för en äldrekursare att ligga eller försöka ligga med en nollaa"/>
          <InfoCard header="N0llefriden" text="Under mottagningen är det absolut förbjudet under några som helst 
            omständigheten för en äldrekursare att ligga eller försöka ligga med en nollaa"/>
          <InfoCard header="N0llefriden" text="Under mottagningen är det absolut förbjudet under några som helst 
            omständigheten för en äldrekursare att ligga eller försöka ligga med en nollaa"/>
        </View>

        <View>
          <Text style={HEADER} text="Ordlista"/>
          <Lexicon/>

        </View>

        <View>
          <Text style={HEADER} text="Bostadslänkar"/>
        </View>

        <View>
          <Text style={HEADER} text="Umeålänkar"/>
        </View>

      </ScrollView>
    </Screen>
  )
})
