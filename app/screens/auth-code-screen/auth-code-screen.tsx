import React, { useState } from "react"
import { observer } from "mobx-react-lite"
import { Image, ImageStyle, TextInput, TextStyle, View, ViewStyle } from "react-native"
import { Button, Screen, Text } from "../../components"
import { color, spacing } from "../../theme"
import { useNavigation } from "@react-navigation/native"
// import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
const sortingHat = require("./sorting-hat.png")

const fontSize = 30

const ROOT: ViewStyle = {
  alignItems: "center",
  flex: 1,
}

const TEXTBOX: TextStyle = {
  color: "#000",
  marginHorizontal: spacing[4],
  borderColor: color.palette.primary,
  borderRadius: 10,
  borderWidth: 3,
  fontSize: fontSize,
  paddingTop: fontSize - 10,
  width: 60,
  height: 80,
  textAlign: "center",
}

const TEXTBOX_CONTAINER: ViewStyle = {
  display: "flex",
  flexDirection: "row",
}

const HIDDEN_INPUT: TextStyle = {
  letterSpacing: 75,
  position: "absolute",
  width: "100%",
  marginLeft: 40,
  fontSize: 30,
  color: color.transparent,
  height: 80,
  zIndex: 10,
  marginTop: spacing[6],
}

const SORTING_HAT: ImageStyle = {
  marginVertical: spacing[8],
  alignSelf: "center",
}

const PERSONAL_CODE_TEXT: TextStyle = {
  paddingLeft: spacing[4],
  paddingBottom: spacing[4],
  color: "#000",
}

const CLEAR_LINK: ViewStyle = {
  alignSelf: "flex-end",
  marginRight: spacing[4],
  marginTop: spacing[4],
}

export const AuthCodeScreen = observer(function AuthCodeScreen() {
  const [code, setCode] = useState("")
  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()
  // OR
  // const rootStore = useStores()

  // Pull in navigation via hook
  const navigation = useNavigation()

  // Klicka för att öppna en textinputfield
  // Men displaya inte dens input, utan displaya från state i 4 lådor
  const displayCode = () => {
    const items = []
    for (let i = 0; i < 4; i++) {
      if (code.charAt(i)) {
        items.push(<Text key={i + code.charAt(i)} style={TEXTBOX} text={code.charAt(i)} />)
      } else {
        items.push(<Text key={i} style={TEXTBOX} text=" " />)
      }
    }
    return <View style={TEXTBOX_CONTAINER}>{items}</View>
  }

  const submit = () => {
    // TODO: run check on database.
    // See if code is in database and see what color it corresponds too
    // Set color in state to that color.

    navigation.navigate("sort")
  }

  return (
    <Screen style={ROOT} preset="fixed">
      <View>
        <Image source={sortingHat} style={SORTING_HAT} />
        <View>
          <TextInput
            caretHidden={false}
            keyboardType="number-pad"
            style={HIDDEN_INPUT}
            defaultValue={code}
            onChangeText={(numbers) => {
              if (numbers.length === 4) submit()
              setCode(numbers)
            }}
          />
          <Text text="Ange din personliga kod" style={PERSONAL_CODE_TEXT} />
          {displayCode()}
          <Button style={CLEAR_LINK} text="clear" onPress={() => setCode("")} preset="link" />
        </View>
      </View>
    </Screen>
  )
})
