import React, { useState } from "react"
import { observer } from "mobx-react-lite"
import { TextStyle, View, ViewStyle } from "react-native"
import { Button, Screen, Text, TextField } from "../../components"
// import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color, spacing } from "../../theme"
import { useNavigation } from "@react-navigation/native"

const PADDING: ViewStyle = {
  marginVertical: spacing[4],
}

const ROOT: ViewStyle = {
  flex: 1,
  justifyContent: "center",
  backgroundColor: color.palette.white,
}

const HEADER: TextStyle = {
  fontSize: 32,
  fontWeight: "bold",
  color: color.palette.black,
}

const HEADER_VIEW: ViewStyle = {
  display: "flex",
  flexDirection: "column",
  marginHorizontal: spacing[6],
}

const INPUTFIELDS: ViewStyle = {
  marginHorizontal: spacing[6],
  marginVertical: spacing[8],
  display: "flex",
  flexDirection: "column",
}

const LOGINBUTTON: ViewStyle = {
  ...PADDING,
  alignSelf: "flex-end",
}

export const LoginScreen = observer(function LoginScreen() {
  const [username, setUsername] = useState("")
  const [password, setPassword] = useState("")
  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()
  // OR
  // const rootStore = useStores()

  const navigation = useNavigation()
  const nextPage = () => navigation.navigate("primaryStack", { screen: "home" })

  const login = () => {
    // Run some logic to login
    console.log(username)
    console.log(password)

    // If authorized
    nextPage()
  }

  return (
    <Screen style={ROOT} preset="fixed">
      <View style={HEADER_VIEW}>
        <Text style={HEADER} text="Välkommen" />
        <Text style={HEADER} text="tillbaka!" />
      </View>
      <View style={INPUTFIELDS}>
        <TextField
          style={PADDING}
          label="Användarnamn"
          placeholder="Användarnamn"
          onChangeText={(text) => setUsername(text)}
          defaultValue={username}
        />
        <TextField
          style={PADDING}
          label="Lösenord"
          placeholder="Lösenord"
          secureTextEntry={true}
          onChangeText={(text) => setPassword(text)}
          defaultValue={password}
        />
        <Button onPress={login} style={LOGINBUTTON} text="Logga in" preset="flat" />
      </View>
    </Screen>
  )
})
