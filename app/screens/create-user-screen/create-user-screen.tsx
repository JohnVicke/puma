import React, { useState } from "react"
import { observer } from "mobx-react-lite"
import { Image, ImageStyle, ScrollView, TextStyle, TouchableOpacity, View, ViewStyle } from "react-native"
import { Button, Screen, Text, TextField } from "../../components"
import * as EmailValidator from "email-validator"
import ImagePicker from "react-native-image-picker"

// import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color, spacing } from "../../theme"
import { useNavigation } from "@react-navigation/native"
const placeholder = require("./placeholder.png")

const AVATAR_CONTAINER: ViewStyle = {
  marginBottom: spacing[6],
}

const PADDING: ViewStyle = {
  marginVertical: spacing[4],
}

const ROOT: ViewStyle = {
  flex: 1,
  justifyContent: "center",
  backgroundColor: color.palette.white,
}

const INPUTFIELDS: ViewStyle = {
  marginHorizontal: spacing[6],
  marginVertical: spacing[8],
  display: "flex",
  flexDirection: "column",
}

const LOGINBUTTON: ViewStyle = {
  ...PADDING,
  alignSelf: "flex-end",
}

export const CreateUserScreen = observer(function CreateUserScreen() {
  const [username, setUsername] = useState("")
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [rePassword, setRePassword] = useState("")
  const [errors, setErrors] = useState([])

  const [avatarSource, setAvatarSource] = useState({ uri: "" })
  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()
  // OR
  // const rootStore = useStores()

  // Pull in navigation via hook
  // const navigation = useNavigation()
  const resetErrors = () => {
    errors.splice(0, errors.length)
    setErrors([...errors])
  }

  const navigation = useNavigation()
  const goTo = (screen: string) => {
    navigation.navigate(screen)
  }
  const validateEmail = () => {
    // @ts-ignore
    return EmailValidator.validate(String(email).toLowerCase())
  }

  const usernameExists = () => {
    if (!username) {
      return true
    }
    return false
  }

  const createUser = () => {
    resetErrors()
    if (password !== rePassword) {
      errors.push({ error: "Passwords do not match", type: "password" })
    }
    if (!validateEmail()) {
      errors.push({ error: "Email is invalid", type: "email" })
    }

    if (usernameExists()) {
      errors.push({ error: "Username already exists", type: "username" })
    }
    setErrors([...errors])
    goTo("authCode");
  }

  const selectPhoto = () => {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
      },
    }

    ImagePicker.showImagePicker(options, (response) => {
      console.log("Response = ", response)

      if (response.didCancel) {
        console.log("User cancelled photo picker")
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error)
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton)
      } else {
        const source = { uri: response.uri }

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };
        setAvatarSource(source)
      }
    })
  }

  const IMG_STYLE: ImageStyle = {
    alignSelf: "center",
    width: 200,
    height: 200,
    borderRadius: 10,
    borderColor: color.palette.primary,
    borderWidth: 2,
    marginBottom: spacing[2],
  }

  const getImage = () => {
    if (avatarSource.uri === "") {
      return <Image source={placeholder} style={IMG_STYLE} />
    } else {
      return <Image source={avatarSource} style={IMG_STYLE} />
    }
  }

  const ERROR: TextStyle = {
    color: "red",
    marginTop: -spacing[2],
    fontSize: 12,
  }

  const resetText = (type: string) => {
    const error = errors.find((x) => x.type === type)
    if (error) {
      errors.splice(errors.indexOf(error), 1)
      setErrors([...errors])
    }
  }

  const setText = (text: string, type: string) => {
    resetText(type)
    switch (type) {
      case "username":
        setUsername(text)
        break
      case "password":
        setPassword(text)
        break
      case "rePassword":
        setRePassword(text)
        break
      case "email":
        setEmail(text)
        break
    }
  }

  const getError = (type: string) => {
    const error = errors.find((x) => x.type === type)
    return error ? <Text text={error.error} style={ERROR} /> : null
  }

  const AVATAR_TEXT: TextStyle = {
    color: color.palette.black,
    alignSelf: "center",
  }

  

  return (
    <Screen style={ROOT} preset="scroll">
      <ScrollView>
        <View style={INPUTFIELDS}>
          <TouchableOpacity onPress={selectPhoto} style={AVATAR_CONTAINER}>
            {getImage()}
            <Text text="Välj avatar" style={AVATAR_TEXT} />
          </TouchableOpacity>
          <TextField
            style={PADDING}
            label="Användarnamn*"
            placeholder="Användarnamn"
            onChangeText={(text) => setText(text, "username")}
            defaultValue={username}
          />
          {getError("username")}
          <TextField
            style={PADDING}
            label="Mejl*"
            placeholder="Mejladdress"
            onChangeText={(text) => setText(text, "email")}
            defaultValue={email}
          />
          {getError("email")}
          <TextField
            style={PADDING}
            label="Lösenord*"
            placeholder="Lösenord"
            secureTextEntry={true}
            onChangeText={(text) => setText(text, "password")}
            defaultValue={password}
          />
          {getError("password")}
          <TextField
            style={PADDING}
            label="Ange lösenord igen*"
            placeholder="Lösenord"
            secureTextEntry={true}
            onChangeText={(text) => setText(text, "rePassword")}
            defaultValue={rePassword}
          />
          {getError("password")}
          <Button onPress={createUser} style={LOGINBUTTON} text="Skapa användare" preset="flat" />
        </View>
      </ScrollView>

    </Screen>
  )
})
