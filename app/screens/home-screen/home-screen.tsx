import React from "react"
import { observer } from "mobx-react-lite"
import { Image, ImageBackground, ImageStyle, ScrollView, TextStyle, View, ViewStyle } from "react-native"
import { BoardMessage, Button, EventCard, Screen, Text } from "../../components"
// import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color, spacing, radius } from "../../theme"
import { useNavigation } from "@react-navigation/native"
import { icons } from "../../components/icon/icons"
const topCurve = require("./top-curve.png")
const profilePic = require("./profile.png")

const ROOT: ViewStyle = {
  backgroundColor: color.palette.white,
  flex: 1,
}
const PROFILE: ViewStyle = {
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  justifyContent: "space-evenly",
}
const PICANDNAME: ViewStyle = {
  display: "flex",
  flexDirection: "column",
  alignItems: "flex-start",
  marginLeft: spacing[7],
  marginTop: spacing[8],
}
const PROFILEINFO: ViewStyle = {
  display: "flex",
  flexDirection: "column",
  alignItems: "flex-start",
  marginLeft: spacing[7],
}
const BOARD: ViewStyle = {
  display: "flex",
  flexDirection: "column",
  alignItems: "flex-start",
  marginHorizontal: spacing[4],
  marginVertical: spacing[4],
  height: 170,
  borderRadius: radius[2],
  backgroundColor: color.palette.lightYellow,
}
const NEXTEVENT: ViewStyle = {
  display: "flex",
  width: "100%",
  alignItems: "center",
}

const LOGOUT: ViewStyle = {
  position: "absolute",
  top: 0,
  right: 10,
}

const TOPIMAGE: ImageStyle = {
  width: "100%",
  height: 350,
  marginBottom: -20,
}
const PROFILEIMAGE: ImageStyle = {
  width: 126,
  height: 126,
  borderRadius: radius[3],
  borderColor: color.palette.white,
  borderWidth: 4,
}
const PROFILENAME: TextStyle = {
  fontWeight: "700",
  fontSize: 18,
  marginTop: 10,
}
const FATTEXT: TextStyle = {
  fontWeight: "600",
  fontSize: 18,
  lineHeight: 40,
}
const NONFATTEXT: TextStyle = {
  fontWeight: "300",
  fontSize: 18,
}
const COLORTEXT: TextStyle = {
  fontWeight: "600",
  fontSize: 16,
  color: color.palette.primary,
  marginLeft: spacing[5],
}
const BUTTONTEXT: TextStyle = {
  fontSize: 13,
  color: color.palette.primary,
  marginTop: -10,
}

export const HomeScreen = observer(function HomeScreen() {
  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()
  // OR
  // const rootStore = useStores()

  // Pull in navigation via hook
  // const navigation = useNavigation()

  const navigation = useNavigation()
  const goTo = (screen: string) => {
    navigation.navigate(screen)
  }
  return (
    <Screen style={ROOT} preset="scroll" unsafe={true}>
        
      <ImageBackground source={topCurve} style={TOPIMAGE}>
        <View style={PICANDNAME}>
          <Button
            preset="icon"
            style={LOGOUT}
            text=""
            appendIcon={true}
            icon={"logout"}
            onPress={() => goTo("contact")}
          ></Button>
          <View style={PROFILE}>
            <Image source={profilePic} style={PROFILEIMAGE} />
            <View style={PROFILEINFO}>
              <Text>
                <Text style={FATTEXT}>Team: </Text>
                <Text style={NONFATTEXT}>Röd</Text>
              </Text>
              <Text>
                <Text style={FATTEXT}>Poäng: </Text>
                <Text style={NONFATTEXT}>420</Text>
              </Text>
            </View>
          </View>

          <Text style={PROFILENAME}>Karl Lambertsson</Text>
        </View>
      </ImageBackground>
      <View>
        <Text style={COLORTEXT}>Anslagstavla:</Text>
        <View style={BOARD}>
          <BoardMessage title={"Anslagstavlan är tom!"} />
        </View>
      </View>
      <View>
        <Text style={COLORTEXT}>Nästa event:</Text>
        <EventCard
          beer={true}
          outdoor={true}
          wine={false}
          date={"2020-11-03T18:00:00"}
          title={"Beerpong med indivID"}
          content={
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
          }
          location={"Tvistevägen 11D"}
        ></EventCard>
        <View style={NEXTEVENT}>
          <Button
            preset="link"
            text="Visa alla event"
            textStyle={BUTTONTEXT}
            onPress={() => goTo("contact")}
          ></Button>
        </View>
      </View>

    </Screen>
  )
})
