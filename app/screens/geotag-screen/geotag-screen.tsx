import React from "react"
import { observer } from "mobx-react-lite"
import { ImageBackground, ImageStyle, TextStyle, ViewStyle } from "react-native"
import { Screen, Text } from "../../components"
// import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color } from "../../theme"

const geoImage = require("./geotag-image.png")
const cloud = require("./cloud.png")

const ROOT: ViewStyle = {
  backgroundColor: color.palette.white,
  flex: 1,
}
const GEOIMAGE: ImageStyle = {
  width: "100%",
  height: "92%",
}
const CLOUDIMAGE: ImageStyle = {
  width: 400,
  height: 200,
}
const GEOTEXT: TextStyle = {
  position: "absolute",
  top: 90,
  left: 100,
  fontWeight: "700",
  fontSize: 26,
  marginTop: 10,
}

export const GeotagScreen = observer(function GeotagScreen() {
  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()
  // OR
  // const rootStore = useStores()

  // Pull in navigation via hook
  // const navigation = useNavigation()
  return (
    <Screen style={ROOT} preset="scroll">
      <ImageBackground source={geoImage} style={GEOIMAGE}>
        <ImageBackground source={cloud} style={CLOUDIMAGE}>
          <Text style={GEOTEXT}>Coming soon...</Text>
        </ImageBackground>
      </ImageBackground>
    </Screen>
  )
})
