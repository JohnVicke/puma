import React, { useState } from "react"
import { observer } from "mobx-react-lite"
import { ScrollView, ViewStyle } from "react-native"
import { Button, EventCard, InfoCard, Screen, Text } from "../../components"
// import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color, spacing } from "../../theme"
import { CalendarList } from "react-native-calendars"
import { months } from "./months"

const ROOT: ViewStyle = {
  backgroundColor: color.palette.white,
  flex: 1,
}

const SCROLL_STYLE: ViewStyle = {
  marginVertical: spacing[4],
  display: "flex",
  flexDirection: "column",
}

const TOGGLE_BUTTON: ViewStyle = {
  alignSelf: "center",
}
const CLOSE_CALENDAR: ViewStyle = {
  alignSelf: "center",
  marginTop: -70,
  marginBottom: spacing[4],
}

export const CalenderScreen = observer(function CalenderScreen() {
  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()
  const [calendar, setCalendar] = useState(false)
  const todaysDate = months[new Date().getMonth()]

  const events = [
    {
      beer: true,
      outdoor: true,
      wine: false,
      date: "2020-10-25T18:00:00",
      title: "Beerpong med indivID",
      content:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
      location: "Tvistevägen 11D",
    },
    {
      beer: false,
      outdoor: false,
      wine: false,
      date: "2020-10-26T18:00:00",
      title: "Lunch med jonte",
      content:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
      location: "Tvistevägen 11D",
    },
    {
      beer: true,
      outdoor: true,
      wine: true,
      date: "2020-10-28T18:00:00",
      title: "Caps med data",
      content:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
      location: "Tvistevägen 11D",
    },
  ]

  const toggleCalendar = () => {
    setCalendar(!calendar)
  }

  const eventList = () => {
    const out = events.map((event) => {
      return (
        <EventCard
          key={event.date}
          beer={event.beer}
          outdoor={event.outdoor}
          wine={event.wine}
          date={event.date}
          title={event.title}
          content={event.content}
          location={event.location}
        />
      )
    })

    return <>{out}</>
  }

  const calendarEvents = () => {
    const obj = {}

    events.forEach((x) => {
      obj[x.date.split("T")[0]] = {
        selected: true,
        marked: true,
        selectedColor: color.palette.primary,
      }
    })

    return obj
  }

  console.log(calendarEvents())
  // OR
  // const rootStore = useStores()

  // Pull in navigation via hook
  // const navigation = useNavigation()
  return (
    <Screen style={ROOT} preset="scroll">
      {!calendar && (
        <Button style={TOGGLE_BUTTON} onPress={toggleCalendar} preset="link" text={todaysDate} />
      )}
      <ScrollView style={SCROLL_STYLE}>
        {calendar && (
          <>
            <CalendarList
              theme={{
                backgroundColor: "#ffffff",
                calendarBackground: "#ffffff",
                textSectionTitleColor: "#b6c1cd",
                textSectionTitleDisabledColor: "#d9e1e8",
                selectedDayBackgroundColor: "#00adf5",
                selectedDayTextColor: "#ffffff",
                todayTextColor: color.palette.primary,
                dayTextColor: "#2d4150",
                textDisabledColor: "#d9e1e8",
                dotColor: "#00adf5",
                selectedDotColor: "#ffffff",
                arrowColor: "orange",
                disabledArrowColor: "#d9e1e8",
                monthTextColor: "blue",
                indicatorColor: "blue",
                textDayFontWeight: "300",
                textMonthFontWeight: "bold",
              }}
              markedDates={calendarEvents()}
              horizontal={true}
              pagingEnabled={true}
              onDayPress={(day) => {
                console.log("selected day", day)
              }}
              disableArrows={false}
              onDayLongPress={(day) => {
                console.log("selected day", day)
              }}
              monthFormat={"yyyy MM"}
              onMonthChange={(month) => {
                console.log("month changed", month)
              }}
              disableAllTouchEventsForDisabledDays={true}
              renderHeader={(date) => {
                /*Return JSX*/
              }}
              hideArrows={true}
              // Do not show days of other months in month page. Default = false
              hideExtraDays={true}
              // If hideArrows=false and hideExtraDays=false do not swich month when tapping on greyed out
              // day from another month that is visible in calendar page. Default = false
              disableMonthChange={true}
            />
            <Button
              preset="roundedFilled"
              text="Close overview"
              onPress={toggleCalendar}
              style={CLOSE_CALENDAR}
            />
          </>
        )}
        {eventList()}
      </ScrollView>
    </Screen>
  )
})
