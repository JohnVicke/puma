import React from "react"
import { observer } from "mobx-react-lite"
import { Image, ImageStyle, TextStyle, View, ViewStyle } from "react-native"
import { Screen, Text } from "../../components"
// import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color, spacing, typography } from "../../theme"
import { useNavigation } from "@react-navigation/native"
const Balls = require("./Balls.png")


const ROOT: ViewStyle = {
  backgroundColor: color.palette.white,
  flex: 1,
}

const HEADER: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 20,
  fontWeight: "bold",
  color: color.palette.black,
  marginBottom: spacing[2]
}

const VIEWGROUP: ViewStyle = {
  display: "flex",
  flexDirection: "column",
  alignContent:"center",
  alignItems:"center",
  justifyContent:"center",
  height: "100%"
}

const BALLS: ImageStyle = {
  marginBottom: spacing[7],
}

export const SortScreen = observer(function SortScreen() {
  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()
  // OR
  // const rootStore = useStores()

  // Pull in navigation via hook
  const navigation = useNavigation()
  const nextPage = () =>{
    navigation.navigate("primaryStack", { screen: "home" })
  } 

  const navigate = () => {
    setTimeout(nextPage ,3000)
  }

  return (
    <Screen style={ROOT} preset="scroll">
      <View style={VIEWGROUP}>
        <Image source={Balls} style={BALLS}/>
        <Text style={HEADER} text="Sorterar in dig i en färg..." />
        {navigate()}
      </View>
    </Screen>
  )
})
