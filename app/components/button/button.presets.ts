import { ViewStyle, TextStyle } from "react-native"
import { color, spacing } from "../../theme"

/**
 * All text will start off looking like this.
 */
const BASE_VIEW: ViewStyle = {
  paddingVertical: spacing[3],
  paddingHorizontal: spacing[4],
  borderRadius: 4,
  justifyContent: "center",
  alignItems: "center",
}

const BASE_TEXT: TextStyle = {
  paddingHorizontal: spacing[3],
  textTransform: "uppercase",
  fontWeight: "bold",
}

const BASE_ROUNDED: ViewStyle = {
  borderRadius: 100,
}

/**
 * All the variations of text styling within the app.
 *
 * You want to customize these to whatever you need in your app.
 */
export const viewPresets = {
  /**
   * A smaller piece of secondard information.
   */
  primary: { ...BASE_VIEW, backgroundColor: color.palette.red } as ViewStyle,

  /**
   * A button without extras.
   */
  link: {
    ...BASE_VIEW,
    paddingHorizontal: 0,
    paddingVertical: 0,
    alignItems: "flex-start",
  } as ViewStyle,

  roundedFilled: {
    ...BASE_VIEW,
    ...BASE_TEXT,
    ...BASE_ROUNDED,
    backgroundColor: color.palette.primary,
  } as ViewStyle,

  flat: {
    ...BASE_VIEW,
    ...BASE_TEXT,
    backgroundColor: color.palette.primary,
  } as ViewStyle,

  roundedOutlined: {
    ...BASE_VIEW,
    ...BASE_TEXT,
    ...BASE_ROUNDED,
    borderColor: color.palette.primary,
    borderStyle: "solid",
    borderWidth: 2,
  } as ViewStyle,

  icon: {
    ...BASE_TEXT,
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  } as ViewStyle,
}

export const textPresets = {
  primary: {
    ...BASE_TEXT,
    fontSize: 16,
    color: color.palette.white,
  } as TextStyle,
  roundedOutlined: {
    ...BASE_TEXT,
    fontSize: 16,
    color: color.palette.red,
  } as TextStyle,
  link: {
    ...BASE_TEXT,
    color: color.palette.black,
    paddingHorizontal: 0,
    paddingVertical: 0,
  } as TextStyle,
}

/**
 * A list of preset names.
 */
export type ButtonPresetNames = keyof typeof viewPresets
