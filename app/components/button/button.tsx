import * as React from "react"
import { TouchableOpacity, View } from "react-native"
import { Text } from "../text/text"
import { viewPresets, textPresets } from "./button.presets"
import { ButtonProps } from "./button.props"
import { mergeAll, flatten } from "ramda"
import { Icon } from "../icon/icon"

/**
 * For your text displaying needs.
 *
 * This component is a HOC over the built-in React Native one.
 */
export function Button(props: ButtonProps) {
  // grab the props
  const {
    preset = "primary",
    tx,
    text,
    style: styleOverride,
    textStyle: textStyleOverride,
    children,
    icon,
    prependIcon,
    appendIcon,
    ...rest
  } = props

  const viewStyle = mergeAll(flatten([viewPresets[preset] || viewPresets.primary, styleOverride]))
  const textStyle = mergeAll(
    flatten([textPresets[preset] || textPresets.primary, textStyleOverride]),
  )

  let content

  if (icon) {
    const iconStyle = mergeAll(flatten([viewPresets.icon]))
    if (prependIcon) {
      iconStyle.flexDirection = "row"
    } else if (appendIcon) {
      iconStyle.flexDirection = "row-reverse"
    }
    content = (
      <View style={iconStyle}>
        <Icon icon={icon} />
        <Text tx={tx} text={text} style={textStyle} />
      </View>
    )
  } else {
    content = children || <Text tx={tx} text={text} style={textStyle} />
  }

  return (
    <TouchableOpacity style={viewStyle} {...rest}>
      {content}
    </TouchableOpacity>
  )
}
