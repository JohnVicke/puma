export const icons = {
  back: require("./arrow-left.png"),
  bullet: require("./bullet.png"),
  home: require("./home.png"),
  contact: require("./contact.png"),
  calendar: require("./calendar.png"),
  info: require("./info.png"),
  game: require("./game.png"),
  homefocused: require("./homefocused.png"),
  contactfocused: require("./contactfocused.png"),
  calendarfocused: require("./calendarfocused.png"),
  infofocused: require("./infofocused.png"),
  gamefocused: require("./gamefocused.png"),
  beer: require("./beer.png"),
  beergray: require("./beer-gray.png"),
  wine: require("./wine.png"),
  winegray: require("./wine-gray.png"),
  outdoor: require("./outdoor.png"),
  outdoorgray: require("./outdoor-gray.png"),
  logout: require("./logout.png"),
  mail: require("./mail.png"),
  callContact: require("./call-calling.png"),

}

export type IconTypes = keyof typeof icons
