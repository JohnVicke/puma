import * as React from "react"
import { TextStyle, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { color, spacing, typography } from "../../theme"
import { BulletItem, Text } from "../"
import { Icon } from "../icon/icon"

const CONTAINER: ViewStyle = {
  justifyContent: "flex-start",
  display: "flex",
  flexDirection: "column",
  //couldn't get palette to work so hardcoded for now.
  backgroundColor: "#EAEAEA",
  borderRadius: 10,
  marginBottom: spacing[3],
}

const TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 14,
  color: color.palette.black,
}

export interface LexiconProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle
}

/**
 * Describe your component here
 */
export const Lexicon = observer(function Lexicon(props: LexiconProps) {
  const { style } = props

  return (
    <View style={[CONTAINER, style]}>
      <BulletItem header="Svingen" text="cykelvägen brevid sjukhuset ner mot stan." border={true} />
      <BulletItem header="Klamydiasjön" text="Den lilla sjön mitt på campus" border={true}/>
      
      <BulletItem header="ÅC" text="Ålidhem centrum, Ica, Rouge och Baloo bland annat" border={true} />
      <BulletItem header="Origo" text="NTK's kårhus, här festar vi!" border={true} />
      <BulletItem header="Caps" text="Drykeslek med kapsyler, viktigt att vinna över datavetarna!" border={true} />
      <BulletItem header="N0lla" text="Benämning på ny student" border={false} /> 
    </View>
  )
})
