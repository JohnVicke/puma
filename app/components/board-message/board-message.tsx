import * as React from "react"
import { Image, TextStyle, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { color, radius, spacing, typography } from "../../theme"
import { Text } from "../"
const pin = require("./pin.png")

const CONTAINER: ViewStyle = {
  justifyContent: "center",
  backgroundColor: color.palette.lightGray,
  display: "flex",
  width: "90%",
  marginLeft: "auto",
  marginRight: "auto",
  marginTop: spacing[4],
  paddingHorizontal: spacing[3],
  paddingBottom: spacing[4],
  borderRadius: radius[1],
}

const PINS: ViewStyle = {
  display: "flex",
  flexDirection: "row",
  justifyContent: "space-between",
  paddingBottom: spacing[1],
}

const TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 14,
  color: color.primary,
  fontWeight: "600",
}

export interface BoardMessageProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle
  title: string
  message?: string
}

/**
 * Describe your component here
 */
export const BoardMessage = observer(function BoardMessage(props: BoardMessageProps) {
  const { style, title, message } = props

  return (
    <View style={[CONTAINER, style]}>
      <View style={PINS}>
        <Image source={pin} />
        <Image source={pin} />
      </View>
      <Text style={TEXT}>{title}</Text>
    </View>
  )
})
