import * as React from "react"
import { TextStyle, View, ViewStyle, Text, Linking, Image, ImageStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { Button } from "../../components"
let UserPic = require("./profilePic.png")

const CONTAINER: ViewStyle = {
  height: 140,
  width: 380,
  borderRadius: 10,
  marginTop: 15,
  padding: 10,
}

const IMAGE_AND_NAME: ViewStyle = {
  flex: 1,
  flexDirection: "row",
  justifyContent: "space-between",
}

const IMAGE: ImageStyle = {
  height: 60,
  width: 60,
  borderRadius: 60,
  overflow: "hidden",
  marginRight: 10,
}

const NAME: TextStyle = {
  flex: 1,
  fontFamily: typography.primary,
  fontSize: 22,
  fontWeight: "bold",
  color: color.palette.white,
  alignSelf: "center",
}

const CONTACT_INFO: ViewStyle = {
  flex: 1,
  flexDirection: "row",
  justifyContent: "space-between",
}

const MAIL_PHONE: ViewStyle = {
  flex: 1,
  justifyContent: "space-evenly",
  overflow: "hidden",
}

const MAIL_BUTTON: TextStyle = {
  fontSize: 10,
}

/**
 * Component that reads in the information about the contacts and creates a informationbox about them.
 */
export const ContactBox = observer(function ContactBox(prop: {
  name: string
  email: string
  phoneNr: string
  color: ViewStyle
  ProfilePic: Image
}) {
  // If the contact has a profile picture, use that instead
  if (prop.ProfilePic !== null) {
    UserPic = prop.ProfilePic
  }

  return (
    <View style={[CONTAINER, prop.color]}>
      <View style={IMAGE_AND_NAME}>
        <Image source={UserPic} style={IMAGE} />
        <Text style={NAME}>{prop.name}</Text>
      </View>
      <View style={CONTACT_INFO}>
        <View style={MAIL_PHONE}>
          <Button
            style={prop.color}
            onPress={() => Linking.openURL(`mailto:${prop.email}`)}
            textStyle={MAIL_BUTTON}
            icon="mail"
            text={prop.email}
          ></Button>
        </View>
        <View style={MAIL_PHONE}>
          <Button
            style={prop.color}
            onPress={() => Linking.openURL(`tel:${prop.phoneNr}`)}
            icon="callContact"
            text={prop.phoneNr}
          ></Button>
        </View>
      </View>
    </View>
  )
})
