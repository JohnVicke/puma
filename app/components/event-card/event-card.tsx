import * as React from "react"
import { ImageStyle, TextStyle, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { color, radius, spacing, typography } from "../../theme"
import { Icon, Text } from "../"

const CONTAINER: ViewStyle = {
  backgroundColor: color.palette.lightGray,
  marginHorizontal: spacing[4],
  marginVertical: spacing[4],
  paddingBottom: spacing[5],
  borderRadius: radius[2],
  display: "flex",
  flexDirection: "column",
}
const TITLEROW: ViewStyle = {
  display: "flex",
  flexDirection: "row",
  justifyContent: "space-between",
  alignItems: "center",
  margin: spacing[3],
}
const CONTENTROW: ViewStyle = {
  display: "flex",
  flexDirection: "row",
  justifyContent: "space-between",
  marginHorizontal: spacing[3],
  width: "70%",
}
const DATECOLUMN: ViewStyle = {
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
  marginRight: spacing[3],
}
const DATE: ViewStyle = {
  backgroundColor: color.palette.darkGray,
  padding: spacing[4],
  borderRadius: radius[1],
}
const ICONS: ViewStyle = {
  display: "flex",
  flexDirection: "row",
  justifyContent: "space-evenly",
}

const TITLE: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 16,
  color: color.primary,
  fontWeight: "600",
}
const DATETEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 16,
  color: color.primary,
  fontWeight: "600",
}
const TIMETEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 14,
  color: color.primary,
  marginTop: spacing[1],
  fontWeight: "500",
}
const CONTENTTEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 14,
  color: color.primary,
  fontWeight: "300",
}
const ICON: ImageStyle = {
  marginHorizontal: spacing[1],
}

export interface EventCardProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle
  beer: boolean
  outdoor: boolean
  wine: boolean
  date: string
  title: string
  content: string
  location: string
}

/**
 * Describe your component here
 */
export const EventCard = observer(function EventCard(props: EventCardProps) {
  const { style, beer, outdoor, wine, date, title, content, location } = props
  return (
    <View style={[CONTAINER, style]}>
      <View style={TITLEROW}>
        <Text style={TITLE}>{title}</Text>
        <View style={ICONS}>
          <Icon icon={beer ? "beer" : "beergray"} style={ICON} />
          <Icon icon={outdoor ? "outdoor" : "outdoorgray"} style={ICON} />
          <Icon icon={wine ? "wine" : "winegray"} style={ICON} />
        </View>
      </View>
      <View style={CONTENTROW}>
        <View style={DATECOLUMN}>
          <View style={DATE}>
            <Text style={DATETEXT}>
              {new Date(date).getDate()}/{new Date(date).getMonth() + 1}
            </Text>
          </View>
          <Text style={TIMETEXT}>
            {new Date(date).getHours() < 10 ? "0" : ""}
            {new Date(date).getHours()}:{new Date(date).getMinutes() < 10 ? "0" : ""}
            {new Date(date).getMinutes()}
          </Text>
        </View>
        <Text style={CONTENTTEXT}>{content}</Text>
      </View>
    </View>
  )
})
