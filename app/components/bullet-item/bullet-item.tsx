import * as React from "react"
import { View, ViewStyle, ImageStyle, TextStyle } from "react-native"
import { Text } from "../text/text"
import { Icon } from "../icon/icon"
import { color, spacing, typography } from "../../theme"


const BULLET_ITEM_NO_BORDER: ViewStyle = {
  display:"flex",
  flexDirection: "row",
  marginTop: spacing[2],
  paddingBottom: spacing[2],
}
const BULLET_ITEM_BORDER: ViewStyle = {
  ...BULLET_ITEM_NO_BORDER,
  borderBottomWidth: 1,
  borderBottomColor: "#C44F61",
}

const BULLET_CONTAINER: ViewStyle = {
  marginRight: spacing[4] - 1,
  marginTop: spacing[2],
}
const BULLET: ImageStyle = {
  width: 8,
  height: 8,
  marginLeft:spacing[4]
}

const TEXT_BOX: ViewStyle = {
  marginRight: spacing[7]
}

const HEADER: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 16,
  fontWeight: "bold",
  color: color.palette.black,
}

const TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 14,
  color: color.palette.black,
}


export interface BulletItemProps {
  /**
   * Header text
   */
  header?: string

  /**
   * Content text
   */
  text?: string

  /**
   * If there should be a bottom border, true/false
   */
  border?: boolean
}

export function BulletItem(props: BulletItemProps) {
  const{
    header,
    text,
    border
  } = props
  
  return (
    <View style={border? BULLET_ITEM_BORDER : BULLET_ITEM_NO_BORDER}>
      <Icon icon="bullet" containerStyle={BULLET_CONTAINER} style={BULLET} />

      <View>
        <View>
          <Text style={HEADER} text={header} />
        </View>
      
        <View style={TEXT_BOX}>
          <Text style={TEXT} text = {text}/>
        </View>
      </View>

    </View>
  )
}
