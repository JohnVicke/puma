import * as React from "react"
import { TextStyle, TouchableOpacity, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { color, spacing, typography } from "../../theme"
import { Text } from "../"
import { Icon } from "../icon/icon"
import { Button } from "../button/button"

const CONTAINER: ViewStyle = {
  justifyContent: "flex-start",
  display: "flex",
  flexDirection: "row",
  backgroundColor: color.palette.lightGray,
  borderRadius: 10,
  marginBottom: spacing[3],
}

const TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 14,
  color: color.palette.black,
}

const HEADER: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 16,
  fontWeight: "bold",
  color: color.palette.black,
}

const CARDTOP: ViewStyle = {
  display: "flex",
  flexDirection: "row",
  justifyContent: "space-between",
  marginVertical: spacing[2],
}

const CARDMID: ViewStyle = {
  display: "flex",
  flexDirection: "row",
  paddingBottom: spacing[2],
  paddingRight: spacing[4],
}

/**
 * Weird ass margin didn't want to work so I forced it
 */
const TEXTCONTAINER: ViewStyle = {
  marginRight: spacing[5],
}

const BULLET: ViewStyle = {
  display: "flex",
  flexDirection: "column",
  alignContent: "flex-start",
  paddingVertical: spacing[4],
  marginHorizontal: spacing[3],
}

/**
 * fix the FRIGGIN button
 */
const BUTTON: ViewStyle = {
  display: "flex",
  alignItems: "center",
  alignContent: "flex-end",
  backgroundColor: color.palette.primary,
  borderRadius: 3,
}

export interface InfoCardProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle

  header?: string

  text?: string

  disableButton?: boolean
}

/**
 * Describe your component here
 */
export const InfoCard = observer(function InfoCard(props: InfoCardProps) {
  const { style, header, text, disableButton } = props

  return (
    <View style={[CONTAINER, style]}>
      <View style={BULLET}>
        <Icon icon="bullet" />
      </View>

      <View style={TEXTCONTAINER}>
        <View style={CARDTOP}>
          <Text style={HEADER} text={header} />
        </View>
        <View style={CARDMID}>
          <Text style={TEXT} text={text} />
        </View>

        {!disableButton && (
          <TouchableOpacity style={BUTTON}>
            <Text text="Gå till hemsidan" />
          </TouchableOpacity>
        )}
      </View>
    </View>
  )
})
