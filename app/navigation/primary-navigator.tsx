/* eslint-disable react/display-name */
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"
/**
 * This is the navigator you will modify to display the logged-in screens of your app.
 * You can use RootNavigator to also display an auth flow or other user flows.
 *
 * You'll likely spend most of your time in this file.
 */
import React from "react"
import { Dimensions, StyleSheet } from "react-native"
import { Icon } from "../components/icon/icon"
import { CalenderScreen, HomeScreen, InfoScreen, WelcomeScreen, ContactScreen, GeotagScreen } from "../screens"
import { color } from "../theme"

/**
 * This type allows TypeScript to know what routes are defined in this navigator
 * as well as what properties (if any) they might take when navigating to them.
 *
 * If no params are allowed, pass through `undefined`. Generally speaking, we
 * recommend using your MobX-State-Tree store(s) to keep application state
 * rather than passing state through navigation params.
 *
 * For more information, see this documentation:
 *   https://reactnavigation.org/docs/params/
 *   https://reactnavigation.org/docs/typescript#type-checking-the-navigator
 */
export type PrimaryParamList = {
  home: undefined
}

// Documentation: https://github.com/software-mansion/react-native-screens/tree/master/native-stack
const DEVICE_WIDTH = Dimensions.get("window").width
const styles = StyleSheet.create({
  icon: {
    height: 31,
    width: 31,
  },
})

const Tab = createBottomTabNavigator()

export function PrimaryNavigator() {
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName
          console.log(route)

          if (route.name === "home") {
            iconName = focused ? "homefocused" : "home"
          } else if (route.name === "contact") {
            iconName = focused ? "contactfocused" : "contact"
          } else if (route.name === "calendar") {
            iconName = focused ? "calendarfocused" : "calendar"
          } else if (route.name === "info") {
            iconName = focused ? "infofocused" : "info"
          } else if (route.name === "game") {
            iconName = focused ? "gamefocused" : "game"
          }

          return <Icon icon={iconName} style={styles.icon} />
        },
      })}
      tabBarOptions={{
        style: {
          backgroundColor: color.palette.primary,
          borderTopEndRadius: 10,
          borderTopStartRadius: 10,
          position: "absolute",
          bottom: 0,
          paddingTop: 10,
          width: DEVICE_WIDTH,
          zIndex: 8,
        },
        showLabel: false,
      }}
    >
      <Tab.Screen name="home" component={HomeScreen} />
      <Tab.Screen name="contact" component={ContactScreen} />
      <Tab.Screen name="calendar" component={CalenderScreen} />
      <Tab.Screen name="info" component={InfoScreen} />
      <Tab.Screen name="game" component={GeotagScreen} />
    </Tab.Navigator>
  )
}

/**
 * A list of routes from which we're allowed to leave the app when
 * the user presses the back button on Android.
 *
 * Anything not on this list will be a standard `back` action in
 * react-navigation.
 *
 * `canExit` is used in ./app/app.tsx in the `useBackButtonHandler` hook.
 */
const exitRoutes = ["welcome"]
export const canExit = (routeName: string) => exitRoutes.includes(routeName)
